installation
--
cloner le projet

aller dans components

lancer le build

`docker-compose build` 

Les conteneurs suivants seront créés
- twp_apache : pour servir sur le port 80 tous les répertoires virtuels pointant sur sources
- twp_php : instance php-fpm qui est appelée par twp_apache pour exécuter vos scripts
- twp_mysql : installation de mysql
- twp_myadmin : phpmyadmin qui pointe sur l'instance mysql (url sur le port 8080)

Le user et le mot de passe de mysql se configurent dans le fichier docker-compose.yml

Le hostname a appeler dans votre code pour mysql est :
`DATABASE_URL=mysql://root:root@mysql:3306/nomdelabase`
 
 
Lancement des Conteneurs
--
`docker-compose up -d`

Ligne de commande pour executer un shell dans php

`docker exec -it twp_php bash`  (note, composer est déjà présent et la racine de votre code se retrouve sur /home/wwwroot



pour mysql

`docker exec -it twp_mysql bash`
 
Arrêt des Conteneurs
--
`docker-compose down` 
